# 1. Github
- Create an account at www.github.com if you do not have one. You can use the one you are currently using if you have
- Create a private repository for the assignment at your GitHub account
- Push the notebook file [assignment_01.ipynb](https://gitlab.com/cau-class/neural-network/2021-2/assignment/-/blob/main/01/assignment_01.ipynb) to the repository you have created for the assignment 
- Save the created repository page as a PDF file and submit it to the Google Classroom

# 2. Github
- Share your private repository for the assignment with the following GitHub accounts:
    - hytae1993
    - hwanid
    - SeonghoonHam
    - jennbang
    - MessaoudiOumaima
    - rechido
- You can share your private repository at the menu `Settings > Manage access > Invite a collaborator`
- Save the page at `Settings > Manage access` as a PDF file and submit it to the Google Classroom

# 3. Colab
- Create an account at Google Colaboratory https://colab.research.google.com
- Connect your repository for the assignment at GitHub to Google Colaboratory
- You can have a look at the notebook file [assignment_01_colab_github_demo.ipynb](https://gitlab.com/cau-class/neural-network/2021-2/assignment/-/blob/main/01/assignment_01_colab_github_demo.ipynb) for how to do it
- Open the notebook file [assignment_01.ipynb](https://gitlab.com/cau-class/neural-network/2021-2/assignment/-/blob/main/01/assignment_01.ipynb) at Google Colaboratory 
- Run each cell in the notebook one after another
- Commit (Save a copy in GitHub) the notebook file [assignment_01.ipynb](https://gitlab.com/cau-class/neural-network/2021-2/assignment/-/blob/main/01/assignment_01.ipynb) at GitHub after running each cell
- Save the notebook after running all the cells as a PDF file and submit it to the Google Classroom
- You need to follow the steps in [the blog](https://velog.io/@s6820w/colab3) in order to avoid errors in plotting figures in the PDF file

# 4. Github History
- Save the history page for the notebook [assignment_01.ipynb](https://gitlab.com/cau-class/neural-network/2021-2/assignment/-/blob/main/01/assignment_01.ipynb) at GitHub as a PDF file with `Portrait` mode and submit it to Google Classroom

# 5. LaTeX
- Create a notebook at Google Colaboratory 
- Add a text cell to the created notebook
- Write the following mathematical expressions using LaTeX and save the notebook as a PDF file and submit it to the Google Classroom
- (1) $` f(x) = a x^2 + b x + c `$
- (2) $` f(x) = \exp(x) `$
- (3) $` f(x) = \frac{a}{b}, \quad a \in \mathbb{R}, b \in \mathbb{R} `$
- (4) $` f(x) = \sum_{i = 1}^{n} x_i, \quad x = (x_1, x_2, \cdots, x_n) `$
- (5) $` f(x) = \int_{\Omega} \sigma(x, t) dt `$
- (6) $` \frac{\partial \mathbb{E}}{\partial w} `$
- (7) $` w^{t+1} = w^{t} - \eta \nabla \mathcal{L}(w^{t}) `$
